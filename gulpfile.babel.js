import path from 'path';
import gulp from 'gulp';
import gutil from 'gulp-util';
import watch from 'gulp-watch';
import batch from 'gulp-batch';
import eslint from 'gulp-eslint';
import changedInPlace from 'gulp-changed-in-place';

import webpack from 'webpack';
import webpackConfig from './webpack.config.babel';
let webpackCompiler = null;

gulp.task('copy:amcharts', () => {
    gulp.src([
        path.resolve(__dirname, 'src/lib/amcharts/**/*')
    ]).pipe(gulp.dest('dist/amcharts'));
});

gulp.task('lint', () => {
    return gulp.src(['src/**/*.js', 'src/**/*.jsx', '!src/lib/**'])
        .pipe(changedInPlace({
            firstPass: true
        }))
        .pipe(eslint({
            configFile: path.resolve(__dirname, `.eslintrc.json`)
        }))
        .pipe(eslint.format());
});

gulp.task('build', ['lint'], callback => {
    if (webpackCompiler === null) {
        webpackCompiler = webpack(webpackConfig);
    }

    webpackCompiler.run((error, stats) => {
        if (error) {
            throw new gutil.PluginError('js', error, {
                showStack: true
            });
        }
        gutil.log('[js]', stats.toString({
            colors: true
        }));
        callback();
    })
});

gulp.task('watch', ['build'], () => {
    watch([
        'src/**/*.js',
        'src/**/*.jsx',
        'src/**/*.json',
        'src/**/*.less',
        'src/**/*.mustache',
        'src/**/*.png',
        'src/**/*.jpg',
        'src/**/*.gif'
    ], batch((events, callback) => {
        gulp.start('build', callback);
    }));
});
