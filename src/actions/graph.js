import {
    SET_GRAPH_TYPE,
    SET_GRAPH_THICKNESS
} from '../constants/actionTypes';

export const setGraphType = type => {
    return {
        type: SET_GRAPH_TYPE,
        graphType: type
    }
};

export const setGraphThickness = thickness => {
    return {
        type: SET_GRAPH_THICKNESS,
        graphThickness: thickness
    }
};
