import {
    SET_COMPARATOR_VIEW
} from '../constants/actionTypes';

export const setComparatorView = view => {
    return {
        type: SET_COMPARATOR_VIEW,
        view: view
    };
};
