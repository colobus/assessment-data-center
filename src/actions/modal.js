import {
    SHOW_MODAL,
    HIDE_MODAL,
    START_MODAL_ANIMATION,
    STOP_MODAL_ANIMATION
} from '../constants/actionTypes';

export const showModal = name => {
    return (dispatch, getState) => {
        if (getState().modal.name === name || getState().modal.animating) {
            console.log('prevented');
            return;
        }

        dispatch({
            type: SHOW_MODAL,
            name: name
        });
    };
};

export const hideModal = () => {
    return (dispatch, getState) => {
        if (getState().modal.name === null || getState().modal.animating) {
            console.log('prevented');
            return;
        }

        dispatch({
            type: HIDE_MODAL
        });
    };
};

export const startModalAnimation = () => {
    return {
        type: START_MODAL_ANIMATION
    };
};

export const stopModalAnimation = () => {
    return {
        type: STOP_MODAL_ANIMATION
    };
};
