import {SET_LANGUAGE} from '../constants/actionTypes';

export const setLanguage = language => {
    return {
        type: SET_LANGUAGE,
        language: language
    };
};
