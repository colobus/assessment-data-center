import store from '../store';

import {randomNumber, randomInteger} from '../helpers/random';

import {
    ADD_PARAM_DATA,
    REMOVE_PARAM_DATA,
    REMOVE_PARAMS_DATA
} from '../constants/actionTypes';

const negativePow = -3;
const positivePow = 4;

const generateParamData = () => {
    let allowNegative = randomInteger(0, 3) === 0,
        round = randomInteger(0, 1) === 0,
        fixed = randomInteger(1, 3),
        pow1 = randomInteger(allowNegative ? negativePow : 0, positivePow),
        pow2 = randomInteger(allowNegative ? negativePow : 0, positivePow),
        minPow = Math.min(pow1, pow2),
        maxPow = Math.max(pow1, pow2),
        bound1 = randomNumber(Math.pow(10, minPow), Math.pow(10, maxPow)),
        bound2 = randomNumber(Math.pow(10, minPow), Math.pow(10, maxPow)),
        min = Math.min(bound1, bound2),
        max = Math.max(bound1, bound2);

    if (round) {
        min = Math.round(min);
        max = Math.round(max);
    }
    else {
        min = parseFloat(min.toFixed(fixed));
        max = parseFloat(max.toFixed(fixed));
    }

    const itemsCount = store.getState().paramsData.time.length;

    let paramData = [];
    for (let i = 0; i < itemsCount; i++) {
        let number = randomNumber(min, max);
        paramData.push(round ? Math.round(number) : parseFloat(number.toFixed(fixed)));
    }

    return paramData;
};

export const addParamData = (id, data) => {
    return {
        type: ADD_PARAM_DATA,
        id: id,
        data: data
    };
};

export const addParamDataDynamic = id => {
    return dispatch => {
        let data = generateParamData();
        dispatch(addParamData(id, data));
        return data;
    };
};

export const removeParamData = id => {
    return {
        type: REMOVE_PARAM_DATA,
        id: id
    };
};

export const removeParamsData = () => {
    return {
        type: REMOVE_PARAMS_DATA
    };
};
