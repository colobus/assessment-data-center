import store from '../store';

import {
    SET_SELECTION_SATELLITE_ID,
    ADD_SELECTION_PARAM,
    REMOVE_SELECTION_PARAM,
    REMOVE_SELECTION_PARAMS,
    TOGGLE_ADD_PARAM_VISIBILITY,
    SET_ADD_PARAM_SENSOR_ID,
    SET_ADD_PARAM_STATION_ID,
    SET_ADD_PARAM_ID
} from '../constants/actionTypes';

const colors = [
    '#67b7dc',
    '#fdd400',
    '#84b761',
    '#cc4748',
    '#cd82ad',
    '#2f4074',
    '#448e4d',
    '#b7b83f',
    '#b9783f',
    '#b93e3d',
    '#913167'
];

let paramIdIncrement = null;
function generateParamId() {
    if (paramIdIncrement === null) {
        const {selectedParams} = store.getState();
        paramIdIncrement = 0;
        selectedParams.forEach(selectedParam => {
            if (selectedParam.id > paramIdIncrement) {
                paramIdIncrement = selectedParam.id;
            }
        });
    }
    return ++paramIdIncrement;
}

function getColor() {
    let colorCountMap = new Array(colors.length).fill(0);

    const {selectedParams} = store.getState();
    for (let i = 0; i < selectedParams.length; i++) {
        let colorIndex = colors.indexOf(selectedParams[i].color);
        colorCountMap[colorIndex]++;
    }

    let minColorIndex = null,
        minColorCount = null;
    for (let i = 0; i < colorCountMap.length; i++) {
        if (minColorIndex === null || colorCountMap[i] < minColorCount) {
            minColorIndex = i;
            minColorCount = colorCountMap[i];
        }
    }

    return colors[minColorIndex === null ? 0 : minColorIndex];
}

export const setSelectedSatelliteId = id => {
    return {
        type: SET_SELECTION_SATELLITE_ID,
        id: id
    };
};

export const addSelectedParam = (id, param) => {
    return {
        type: ADD_SELECTION_PARAM,
        id,
        sensorId: param.sensorId,
        stationId: param.stationId,
        paramId: param.id,
        color: getColor()
    };
};

export const addSelectedParamDynamic = param => {
    return dispatch => {
        let id = generateParamId();
        dispatch(addSelectedParam(id, param));
        return id;
    };
};

export const removeSelectedParam = id => {
    return {
        type: REMOVE_SELECTION_PARAM,
        id: id
    }
};

export const removeSelectedParams = () => {
    return {
        type: REMOVE_SELECTION_PARAMS
    };
};

export const toggleAddParamVisibility = visible => {
    return {
        type: TOGGLE_ADD_PARAM_VISIBILITY,
        visible: visible
    };
};

export const setAddParamSensorId = id => {
    return {
        type: SET_ADD_PARAM_SENSOR_ID,
        id: id
    };
};

export const setAddParamStationId = id => {
    return {
        type: SET_ADD_PARAM_STATION_ID,
        id: id
    };
};

export const setAddParamId = id => {
    return {
        type: SET_ADD_PARAM_ID,
        id: id
    };
};
