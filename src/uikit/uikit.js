import 'uikit/src/js/core/core';
import 'uikit/src/js/core/dropdown';
import 'uikit/src/js/core/modal';

const uikit = window.UIkit;

export default uikit;
