import store from './store';
import watch from 'redux-watch';
import i18next from 'i18next';

import en from './translations/en.json';
import ru from './translations/ru.json';

i18next.init({
    whitelist: ['en', 'ru'],
    lng: store.getState().language,
    fallbackLng: 'en',
    lowerCaseLng: true,
    debug: NODE_ENV === 'development',
    resources: {
        en: en,
        ru: ru
    },
    interpolation: {
        escapeValue: false // not needed for React
    }
});

let watcher = watch(store.getState, 'language');
store.subscribe(watcher(language => {
    i18next.changeLanguage(language);
}));

export default i18next;
