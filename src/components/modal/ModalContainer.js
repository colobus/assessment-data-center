import {connect} from 'react-redux';

import Modal from './Modal';

import {hideModal} from '../../actions/modal';

import SettingsContainer from '../settings/SettingsContainer';

const mapStateToProps = state => {
    let Component;
    switch (state.modal.name) {
        case 'settings': {
            Component = SettingsContainer;
            break;
        }

        default: {
            Component = null;
            break;
        }
    }

    return {
        Component: Component,
        name: state.modal.name,
        open: state.modal.name !== null
    };
};

const mapDispatchToProps = dispatch => {
    return {
        hide: () => {
            dispatch(hideModal());
        }
    };
};

const ModalContainer = connect(mapStateToProps, mapDispatchToProps)(Modal);

export default ModalContainer;
