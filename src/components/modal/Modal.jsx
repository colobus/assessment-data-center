import React, {Component, PropTypes} from 'react';

import store from '../../store';

import $ from 'jquery';
import UIkit from '../../uikit/uikit';

import {
    hideModal,
    startModalAnimation,
    stopModalAnimation
} from '../../actions/modal';

class Modal extends Component {
    componentDidMount() {
        this.patch();

        $(this.element).on({
            'before:show.uk.modal': () => {
                store.dispatch(startModalAnimation());
            },
            'show.uk.modal': () => {
                store.dispatch(stopModalAnimation());
            },
            'before:hide.uk.modal': () => {
                store.dispatch(startModalAnimation());
            },
            'hide.uk.modal': () => {
                store.dispatch(stopModalAnimation());
                store.dispatch(hideModal());
            }
        });

        this.validate();
    }

    componentWillUnmount() {
        $(this.element).off('show.uk.modal').off('hide.uk.modal');
    }

    componentDidUpdate() {
        this.validate();
    }

    patch() {
        // Monkeypatching modal
        const modal = UIkit.modal(this.element);

        const _hide = modal._hide;
        modal._hide = function() {
            this.trigger('before:hide.uk.modal');
            _hide.apply(modal, arguments);
        };

        const show = modal.show;
        modal.show = function() {
            this.trigger('before:show.uk.modal');
            show.apply(modal, arguments);
        }
    }

    validate() {
        if (store.getState().modal.animating) {
            return;
        }

        const modal = UIkit.modal(this.element);
        if (this.props.open) {
            if (!modal.isActive()) {
                modal.show();
            }
        }
        else {
            if (modal.isActive()) {
                modal.hide();
            }
        }
    }

    render() {
        const Component = this.props.Component;
        const component = Component ? (<Component hide={this.props.hide} />) : '';

        return (
            <div className="uk-modal" ref={element => this.element = element}>
                {component}
            </div>
        );
    }
}

Modal.propTypes = {
    open: PropTypes.bool.isRequired,
    hide: PropTypes.func.isRequired
};

export default Modal;
