import {connect} from 'react-redux';

import Comparator from './Comparator';

const mapStateToProps = state => {
    return {
        active: state.selectedParams.length > 0
    };
};

const ComparatorContainer = connect(mapStateToProps)(Comparator);

export default ComparatorContainer;
