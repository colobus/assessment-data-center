import {connect} from 'react-redux';

import TableView from './TableView';

const mapStateToProps = state => {
    const paramsData = state.paramsData;

    let columns = [];
    for (let key in paramsData) {
        if (!paramsData.hasOwnProperty(key) || key === 'time') {
            continue;
        }

        const id = parseInt(key, 10);
        const param = state.selectedParams.find(param => {
            return param.id === id;
        });
        if (!param) {
            continue;
        }

        columns.push({
            id: id,
            color: param.color,
            values: paramsData[key]
        });
    }

    return {
        time: paramsData.time || [],
        columns: columns
    }
};

const TableViewContainer = connect(mapStateToProps)(TableView);

export default TableViewContainer;
