import React, {PropTypes} from 'react';

import UnitedChartViewContainer from './UnitedChartViewContainer';
import StackedChartViewContainer from './StackedChartViewContainer';
import TableViewContainer from './TableViewContainer';

const Views = ({ activeView }) => (
    <div className="comparator-views">
        <UnitedChartViewContainer active={activeView === 'united'} />
        <StackedChartViewContainer active={activeView === 'stacked'} />
        <TableViewContainer active={activeView === 'table'} />
    </div>
);

Views.propTypes = {
    activeView: PropTypes.string.isRequired
};

export default Views;
