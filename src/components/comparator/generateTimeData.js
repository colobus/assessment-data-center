import moment from 'moment';

const generateTimeData = () => {
    const step = 2400;
    const endMoment = moment();
    const startMoment = moment(endMoment).subtract(1, 'days').toDate();

    let timeData = [];

    for (
        let currentMoment = moment(startMoment);
        currentMoment.isSameOrBefore(endMoment);
        currentMoment.add(step, 'seconds')
    ) {
        timeData.push(currentMoment.toDate());
    }

    return timeData;
};

export default generateTimeData;
