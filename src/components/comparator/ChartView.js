import _difference from 'lodash/difference';
import _keys from 'lodash/keys';
import {Component, PropTypes} from 'react';

import store from '../../store';
import watch from 'redux-watch';

import {addParamData} from '../../actions/paramsData';

import generateTimeData from './generateTimeData';

class ChartView extends Component {
    constructor(props) {
        super(props);
        this.chart = null;
    }

    componentDidMount() {
        let data = store.getState().paramsData;
        if (!data.time) {
            store.dispatch(addParamData('time', generateTimeData()));
        }
        this.updateGraphData(this.props.data, {});

        let graphTypeWatcher = watch(store.getState, 'graph');
        this.graphTypeUnsubscribe = store.subscribe(graphTypeWatcher(graph => {
            if (this.chart !== null) {
                this.updateGraphs(graph);
            }
        }));
    }

    componentWillUpdate(nextProps) {
        this.updateGraphData(nextProps.data, this.props.data);
    }

    componentWillUnmount() {
        this.destroyChart();

        if (this.graphTypeUnsubscribe) {
            this.graphTypeUnsubscribe();
            delete this.graphTypeUnsubscribe;
        }
    }

    createChart(callback) { // eslint-disable-line
        throw new Error('Method "createChart" is not implemented.');
    }

    destroyChart() {
        if (!this.chart) {
            return;
        }

        this.chart.clear();
        delete this.chart;
    }

    updateGraphData(newData, oldData) {
        const chart = this.chart;

        let newKeys = _keys(newData),
            oldKeys = _keys(oldData);

        let removeIds = _difference(oldKeys, newKeys, ['time']),
            addIds = _difference(newKeys, oldKeys, ['time']);

        if (removeIds.length === 0 && addIds.length === 0) {
            return;
        }

        if (!chart && addIds.length > 0) {
            this.createChart(() => {
                this.updateGraphData(newData, oldData);
            });
            return;
        }

        if (removeIds.length > 0) {
            this.cleanData(removeIds);
            for (let i = 0; i < removeIds.length; i++) {
                let id = parseInt(removeIds[i], 10);
                this.removeParamGraph(id);
            }
        }

        if (addIds.length > 0) {
            let {selectedParams} = store.getState();
            this.extendData(addIds);
            for (let i = 0; i < addIds.length; i++) {
                let id = parseInt(addIds[i], 10);
                let selectedParam = selectedParams.find(param => {
                    return param.id === id;
                });
                this.addParamGraph(id, selectedParam.color);
            }
        }

        if (_difference(newKeys, ['time']).length === 0) {
            this.destroyChart();
        }
        else {
            chart.validateNow(true, true);
        }
    }

    updateGraphs(graph) { // eslint-disable-line
        throw new Error('Method "updateGraphs" is not implemented.');
    }

    cleanData(ids) { // eslint-disable-line
        throw new Error('Method "cleanData" is not implemented.');
    }

    extendData(ids) { // eslint-disable-line
        throw new Error('Method "extendData" is not implemented.');
    }

    addParamGraph(id, color) { // eslint-disable-line
        throw new Error('Method "addParamGraph" is not implemented.');
    }

    removeParamGraph(id) { // eslint-disable-line
        throw new Error('Method "removeParamGraph" is not implemented.');
    }
}

ChartView.getGraphType = type => {
    switch (type) {
        case 'smoothed': {
            return 'smoothedLine';
        }

        case 'straight':
        default: {
            return 'line';
        }
    }
};

ChartView.getGraphThickness = thickness => {
    switch (thickness) {
        case 'thin': {
            return 1;
        }

        case 'medium':
        default: {
            return 2;
        }

        case 'fat': {
            return 3;
        }
    }
};

ChartView.propTypes = {
    graphType: PropTypes.string.isRequired,
    graphThickness: PropTypes.string.isRequired
};

export default ChartView;
