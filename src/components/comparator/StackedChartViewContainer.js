import {connect} from 'react-redux';

import StackedChartView from './StackedChartView';

const mapStateToProps = state => {
    return {
        graphType: state.graph.type,
        graphThickness: state.graph.thickness,
        data: state.paramsData
    }
};

const StackedChartViewContainer = connect(mapStateToProps)(StackedChartView);

export default StackedChartViewContainer;
