import store from '../../store';

import _isEmpty from 'lodash/isEmpty';
import _intersection from 'lodash/intersection';

export const extendDataProvider = (dataProvider, keys) => {
    let data = store.getState().paramsData;
    if (!data.time) {
        return dataProvider;
    }

    let isEmpty = _isEmpty(dataProvider);

    keys = keys ? _intersection(keys, Object.keys(data)) : Object.keys(data);
    if (keys.length === 0) {
        return dataProvider;
    }

    for (let i = 0; i < data.time.length; i++) {
        let item = isEmpty ? {} : dataProvider[i];
        for (let j = 0; j < keys.length; j++) {
            let key = keys[j];
            item['field-' + key] = data[key][i];
        }

        if (isEmpty) {
            dataProvider.push(item);
        }
    }

    return dataProvider;
};

export const cleanDataProvider = (dataProvider, keys) => {
    if (keys.length === 0) {
        return dataProvider;
    }

    for (let i = 0; i < dataProvider.length; i++) {
        for (let j = 0; j < keys.length; j++) {
            let key = keys[j];
            if (dataProvider['field-' + key]) {
                delete dataProvider['field-' + key];
            }
        }
    }

    return dataProvider;
};
