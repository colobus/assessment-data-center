import React, {PropTypes} from 'react';

import ViewsContainer from './ViewsContainer';
import ViewButtonsContainer from './ViewButtonsContainer';

import './comparator.less';

const Comparator = ({ active }) => (
    <div className={'comparator' + (active ? ' active' : '')}>
        <ViewsContainer />
        <ViewButtonsContainer />
    </div>
);

Comparator.propTypes = {
    active: PropTypes.bool.isRequired
};

export default Comparator;
