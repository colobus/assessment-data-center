import React, {PropTypes} from 'react';
import moment from 'moment';

import AmCharts from 'amcharts/amcharts';
import 'amcharts/serial';
import 'amcharts/amstock';
import 'amcharts/themes/light';
//import 'amcharts/plugins/export/export';

AmCharts.useUTC = true;

import {extendDataProvider, cleanDataProvider} from './dataProvider';

import ChartView from './ChartView';

class StackedChartView extends ChartView {
    createChart(callback) {
        if (this.chart) {
            return;
        }

        this.chart = AmCharts.makeChart(this.chartElement, {
            type: 'stock',
            theme: 'light',
            listeners: [{
                event: 'init',
                method: () => {
                    if (typeof callback === 'function') {
                        setTimeout(callback, AmCharts.updateRate);
                    }
                }
            }],
            dataSets: [{
                dataProvider: extendDataProvider([], ['time']),
                fieldMappings: [{
                    fromField: 'time',
                    toField: 'field-time'
                }],
                categoryField: 'field-time'
            }],
            panels: [],
            chartScrollbarSettings: {
                position: 'bottom',
                color: '#000000',
                height: 25,
                dragIconWidth: 25,
                dragIconHeight: 25,
            },
            chartCursorSettings: {
                categoryBalloonFunction: value => {
                    return moment.utc(value).format('YYYY-MM-DD HH:mm:ss');
                },
                cursorPosition: 'mouse',
                zoomable: false,
                valueBalloonsEnabled: true
            },
            categoryAxesSettings: {
                labelsEnabled: false,
                parseDates: true,
                dateFormats:  [{
                    period: 'fff',
                    format: 'JJ:NN:SS'
                }, {
                    period: 'ss',
                    format: 'JJ:NN:SS'
                }, {
                    period: 'mm',
                    format: 'JJ:NN'
                }, {
                    period: 'hh',
                    format: 'JJ:NN'
                }, {
                    period: 'DD',
                    format: 'DD.MM.YYYY'
                }, {
                    period: 'WW',
                    format: 'DD.MM.YYYY'
                }, {
                    period: 'MM',
                    format: 'MM.YYYY'
                }, {
                    period: 'YYYY',
                    format: 'YYYY'
                }],
                groupToPeriods: ['ss'],
                minPeriod: 'ss',
                axisColor: '#dadada',
                minorGridEnabled: true
            },
            panelsSettings: {
                creditsPosition: 'bottom-right',
                thousandsSeparator: '',
            }
        });
    }

    updateGraphs(options) {
        const chart = this.chart;

        for (let i = 0; i < chart.panels.length; i++) {
            let panel = chart.panels[i],
                graph = panel.graphs[0];
            graph.type = ChartView.getGraphType(options.type);
            graph.lineThickness = ChartView.getGraphThickness(options.thickness);
        }

        chart.validateNow();
    }

    cleanData(ids) {
        let dataSet = this.chart.dataSets[0];
        cleanDataProvider(dataSet.dataProvider, ids);

        let fieldMappings = dataSet.fieldMappings;
        for (let i = 0; i < ids.length; i++) {
            let id = ids[i],
                index = fieldMappings.findIndex(fieldMapping => {
                    return fieldMapping.fromField === 'field-' + id;
                });

            if (index === -1) {
                continue;
            }

            fieldMappings.splice(index, 1);
        }
    }

    extendData(ids) {
        let dataSet = this.chart.dataSets[0];
        extendDataProvider(dataSet.dataProvider, ids);

        let fieldMappings = dataSet.fieldMappings;
        for (let i = 0; i < ids.length; i++) {
            let id = ids[i];
            if (fieldMappings.findIndex(fieldMapping => {
                return fieldMapping.fromField === 'field-' + id;
            }) >= 0) {
                continue;
            }

            fieldMappings.push({
                fromField: 'field-' + id,
                toField: 'field-' + id
            });
        }
    }

    addParamGraph(id, color) {
        const chart = this.chart;

        let title = '',
            percentHeight = 100 / (chart.panels.length + 1);

        let panel = new AmCharts.StockPanel();
        panel.id = 'p-' + id;
        panel.title = title;
        panel.showCategoryAxis = true;
        panel.creditsPosition = 'bottom-right';
        panel.stockLegend = {
            enabled: false
        };
        panel.percentHeight = percentHeight;

        let graph = new AmCharts.StockGraph();
        graph.id = 'g-' + id;
        graph.title = title;
        graph.type = ChartView.getGraphType(this.props.graphType);
        graph.valueField = 'field-' + id;
        graph.useDataSetColors = false;
        graph.lineColor = color;
        graph.lineThickness = ChartView.getGraphThickness(this.props.graphThickness);
        graph.bullet = 'round';
        graph.bulletBorderThickness = 1;
        graph.hideBulletsCount = 30;
        panel.addStockGraph(graph);

        chart.addPanel(panel);

        for (let i = 0; i < chart.panels.length - 1; i++) {
            let panel = chart.panels[i];
            panel.showCategoryAxis = false;
            panel.percentHeight = percentHeight;
        }
    }

    removeParamGraph(id) {
        const chart = this.chart;

        let panel = chart.panels.find(panel => {
            return panel.id === 'p-' + id;
        });

        if (!panel) {
            return;
        }

        chart.removePanel(panel);

        let percentHeight = 100 / chart.panels.length;

        for (let i = 0; i < chart.panels.length; i++) {
            let panel = chart.panels[i];
            panel.showCategoryAxis = i + 1 === chart.panels.length;
            panel.percentHeight = percentHeight;
        }
    }

    render() {
        return (
            <div className={'comparator-view comparator-view-margin' + (this.props.active ? ' active' : '')}>
                <div className="comparator-chart" ref={chartElement => this.chartElement = chartElement}></div>
            </div>
        );
    }
}

StackedChartView.propTypes = {
    active: PropTypes.bool.isRequired,
    data: PropTypes.object.isRequired
};

export default StackedChartView;
