import React, {PropTypes} from 'react';

import ViewButton from './ViewButton';

const views = ['united', 'stacked', 'table'];

const ViewButtons = ({ activeView, onButtonClick }) => (
    <div className="comparator-view-buttons uk-button-group">
        {views.map(view => (
            <ViewButton
                key={view}
                view={view}
                activeView={activeView}
                onClick={onButtonClick}
            />
        ))}
    </div>
);

ViewButtons.propTypes = {
    activeView: PropTypes.string.isRequired,
    onButtonClick: PropTypes.func.isRequired
};

export default ViewButtons;
