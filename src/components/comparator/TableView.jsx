import React, {PropTypes} from 'react';

import TableViewHeader from './TableViewHeader';
import TableViewBody from './TableViewBody';

import getScrollbarWidth from '../../helpers/getScrollbarWidth';

const TableView = ({ active, time, columns }) => {
    const headerColumns = columns.map(column => {
        return {
            id: column.id,
            color: column.color
        }
    });

    const headerTableContainerStyle = {
        right: getScrollbarWidth() + 'px'
    };

    return (
        <div className={ 'comparator-view' + (active ? ' active' : '')}>
            <div className="comparator-table-container">
                <div className="comparator-table-header-container" style={headerTableContainerStyle}>
                    <TableViewHeader columns={headerColumns} />
                </div>
                <div className="comparator-table-body-container">
                    <TableViewBody time={time} columns={columns} />
                </div>
            </div>
        </div>
    )
};

TableView.propTypes = {
    active: PropTypes.bool.isRequired,
    time: PropTypes.array.isRequired,
    columns: PropTypes.arrayOf(PropTypes.shape({
        id: PropTypes.number.isRequired,
        color: PropTypes.string.isRequired,
        values: PropTypes.arrayOf(PropTypes.number.isRequired)
    })).isRequired
};

export default TableView;
