import React, {PropTypes} from 'react';

const TableViewHeader = ({ columns }) => {
    const columnStyle = {
        width: 100 / columns.length + '%'
    };

    return (
        <table className="comparator-table-header uk-table">
            <thead>
                <tr>
                    <th className="comparator-table-time"></th>
                    {columns.map(column => {
                        const style = Object.assign({}, columnStyle, {
                            backgroundColor: column.color
                        });
                        return (
                            <th key={column.id} className="comparator-table-value" style={style}></th>
                        );
                    })}
                </tr>
            </thead>
        </table>
    );
};

TableViewHeader.propTypes = {
    columns: PropTypes.arrayOf(PropTypes.shape({
        id: PropTypes.number.isRequired,
        color: PropTypes.string.isRequired
    })).isRequired
};

export default TableViewHeader;
