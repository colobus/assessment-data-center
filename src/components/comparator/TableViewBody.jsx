import React, {PropTypes} from 'react';

import moment from 'moment';

const TableViewBody = ({ time, columns }) => {
    const columnStyle = {
        width: 100 / columns.length + '%'
    };

    return (
        <table className="comparator-table-body uk-table">
            <tbody>
            {time.map((timeItem, i) => {
                let columnsRow = [];
                for (let j = 0; j < columns.length; j++) {
                    let column = columns[j];
                    columnsRow.push({
                        id: column.id,
                        color: column.color,
                        value: column.values[i]
                    });
                }

                return (
                    <tr key={timeItem.getTime()}>
                        <td className="comparator-table-time">{moment.utc(timeItem).format('YYYY-MM-DD HH:mm:ss')}</td>
                        {columnsRow.map(columnItem => {
                            const hightlighterStyle = {
                                backgroundColor: columnItem.color
                            };
                            return (
                                <td key={columnItem.id} className="comparator-table-value" style={columnStyle}>
                                    <span>{columnItem.value}</span>
                                    <span className="comparator-table-highlighter" style={hightlighterStyle}></span>
                                </td>
                            );
                        })}
                    </tr>
                );
            })}
            </tbody>
        </table>
    );
};

TableViewBody.propTypes = {
    time: PropTypes.array.isRequired,
    columns: PropTypes.arrayOf(PropTypes.shape({
        id: PropTypes.number.isRequired,
        values: PropTypes.array.isRequired
    }))
};

export default TableViewBody;
