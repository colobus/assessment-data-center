import React, {PropTypes} from 'react';
import {translate} from 'react-i18next';

const ViewButton = props => {
    const {t} = props;

    return (
        <button
            className={ 'uk-button' + (props.view === props.activeView ? ' uk-button-primary' : '') }
            onClick={() => props.onClick(props.view)}
        >
            {t(props.view)}
        </button>
    );
};

ViewButton.propTypes = {
    view: PropTypes.string.isRequired,
    activeView: PropTypes.string.isRequired,
    onClick: PropTypes.func.isRequired
};

export default translate(['comparator'], { wait: true})(ViewButton);
