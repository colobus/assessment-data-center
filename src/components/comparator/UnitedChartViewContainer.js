import {connect} from 'react-redux';

import UnitedChartView from './UnitedChartView';

const mapStateToProps = state => {
    return {
        graphType: state.graph.type,
        graphThickness: state.graph.thickness,
        data: state.paramsData
    };
};

const UnitedChartViewContainer = connect(mapStateToProps)(UnitedChartView);

export default UnitedChartViewContainer;
