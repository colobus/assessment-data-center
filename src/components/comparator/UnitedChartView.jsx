import React, {PropTypes} from 'react';
import moment from 'moment';

import AmCharts from 'amcharts/amcharts';
import 'amcharts/serial';
import 'amcharts/themes/light';
//import 'amcharts/plugins/export/export';

AmCharts.useUTC = true;

import {extendDataProvider, cleanDataProvider} from './dataProvider';

import ChartView from './ChartView';

const valueAxisOffsetStep = 60;

class UnitedChartView extends ChartView {
    createChart(callback) {
        if (this.chart) {
            return;
        }

        this.chart = AmCharts.makeChart(this.chartElement, {
            type: 'serial',
            theme: 'light',
            listeners: [{
                event: 'init',
                method: () => {
                    if (typeof callback === 'function') {
                        setTimeout(callback, AmCharts.updateRate);
                    }
                }
            }],
            dataProvider: extendDataProvider([], ['time']),
            valueAxes: [],
            graphs: [],
            legend: {
                useGraphSettings: true
            },
            synchronizeGrid: true,
            chartScrollbar: {
                oppositeAxis: false,
                scrollbarHeight: 20,
                offset: 30
            },
            chartCursor: {
                categoryBalloonFunction: value => {
                    return moment.utc(value).format('YYYY-MM-DD HH:mm:ss');
                },
                cursorPosition: 'mouse',
                zoomable: false
            },
            categoryField: 'field-time',
            categoryAxis: {
                parseDates: true,
                dateFormats: [{
                    period: 'fff',
                    format: 'JJ:NN:SS'
                }, {
                    period: 'ss',
                    format: 'JJ:NN:SS'
                }, {
                    period: 'mm',
                    format: 'JJ:NN'
                }, {
                    period: 'hh',
                    format: 'JJ:NN'
                }, {
                    period: 'DD',
                    format: 'DD.MM.YYYY'
                }, {
                    period: 'WW',
                    format: 'DD.MM.YYYY'
                }, {
                    period: 'MM',
                    format: 'MM.YYYY'
                }, {
                    period: 'YYYY',
                    format: 'YYYY'
                }],
                minPeriod: 'ss',
                axisColor: '#dadada',
                boldPeriodBeginning: false,
                minorGridEnabled: true
            },
            thousandsSeparator: '',
            zoomOutOnDataUpdate: false,
            zoomOutText: '',    // https://www.amcharts.com/kbase/custom-zoom-out-button/
            creditsPosition: 'bottom-right'
        });
    }

    updateGraphs(options) {
        const chart = this.chart;

        for (let i = 0; i < chart.graphs.length; i++) {
            let graph = chart.graphs[i];
            graph.type = ChartView.getGraphType(options.type);
            graph.lineThickness = ChartView.getGraphThickness(options.thickness);
        }

        chart.validateNow();
    }

    cleanData(ids) {
        cleanDataProvider(this.chart.dataProvider, ids);
    }

    extendData(ids) {
        extendDataProvider(this.chart.dataProvider, ids);
    }

    addParamGraph(id, color) {
        const chart = this.chart;

        let position = 'left',
            offset = (chart.valueAxes.length - 1) * valueAxisOffsetStep,
            title = '';

        // Add value axis
        let axis = new AmCharts.ValueAxis();
        axis.id = 'va-' + id;
        axis.axisColor = color;
        axis.axisThickness = 2;
        axis.gridAlpha = 0;
        axis.axisAlpha = 1;
        axis.position = position;
        axis.offset = offset;
        chart.addValueAxis(axis);

        // Add graph attached to the axis
        let graph = new AmCharts.AmGraph();
        graph.id = 'g-' + id;
        graph.title = title;
        graph.type = ChartView.getGraphType(this.props.graphType);
        graph.valueAxis = axis;
        graph.valueField = 'field-' + id;
        graph.lineColor = color;
        graph.lineThickness = ChartView.getGraphThickness(this.props.graphThickness);
        graph.bullet = 'round';
        graph.bulletBorderThickness = 1;
        graph.hideBulletsCount = 30;
        chart.addGraph(graph);
    }

    removeParamGraph(id) {
        const chart = this.chart;

        // Removing value axis and graph linked with it
        let isRemoved = false;
        for (let i = 0; i < chart.valueAxes.length; i++) {
            if (isRemoved) {
                chart.valueAxes[i].offset -= valueAxisOffsetStep;
                continue;
            }

            let axis = chart.valueAxes[i];
            if (axis.id !== 'va-' + id) {
                continue;
            }

            chart.removeValueAxis(axis);
            isRemoved = true;
            i--;
        }
    }

    render() {
        return (
            <div className={'comparator-view' + (this.props.active ? ' active' : '')}>
                <div className="comparator-chart" ref={chartElement => this.chartElement = chartElement}></div>
            </div>
        );
    }
}

UnitedChartView.propTypes = {
    active: PropTypes.bool.isRequired,
    data: PropTypes.object.isRequired
};

export default UnitedChartView;
