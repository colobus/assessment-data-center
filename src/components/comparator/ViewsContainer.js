import {connect} from 'react-redux';

import Views from './Views';

const mapStateToProps = state => {
    return {
        activeView: state.comparatorView
    };
};

const ViewsContainer = connect(mapStateToProps)(Views);

export default ViewsContainer;
