import {connect} from 'react-redux';

import ViewButtons from './ViewButtons';

import {setComparatorView} from '../../actions/comparator';

const mapStateToProps = state => {
    return {
        activeView: state.comparatorView
    };
};

const mapDispatchToProps = dispatch => {
    return {
        onButtonClick: view => {
            dispatch(setComparatorView(view));
        }
    };
};

const ViewButtonsContainer = connect(mapStateToProps, mapDispatchToProps)(ViewButtons);

export default ViewButtonsContainer;
