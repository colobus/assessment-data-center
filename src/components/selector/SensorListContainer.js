import {connect} from 'react-redux';
import i18next from 'i18next';

import SensorList from './SensorList';

import {setAddParamSensorId} from '../../actions/selection';

const getSensors = (satellites, satelliteId) => {
    let sensors = [];

    let satellite = satellites.find(satellite => {
        return satellite.id === satelliteId;
    });

    if (satellite) {
        sensors = satellite.sensors || [];
    }

    return [{
        id: 0,
        name: i18next.t('selector:select-sensor')
    }].concat(sensors);
};

const mapStateToProps = state => {
    return {
        sensors: getSensors(state.satellites, state.selectedSatelliteId),
        sensorId: state.addParam.sensorId
    };
};

const mapDispatchToProps = dispatch => {
    return {
        onChange: id => {
            dispatch(setAddParamSensorId(id));
        }
    };
};

const SensorListContainer = connect(mapStateToProps, mapDispatchToProps)(SensorList);

export default SensorListContainer;
