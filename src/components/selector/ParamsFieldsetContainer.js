import {connect} from 'react-redux';

import ParamsFieldset from './ParamsFieldset';

import {toggleAddParamVisibility} from '../../actions/selection';

const mapStateToProps = state => {
    return {
        showAddParam: state.addParam.show
    };
};

const mapDispatchToProps = dispatch => {
    return {
        onAddClick: () => {
            dispatch(toggleAddParamVisibility(true));
        }
    };
};

const ParamsFieldsetContainer = connect(mapStateToProps, mapDispatchToProps)(ParamsFieldset);

export default ParamsFieldsetContainer;
