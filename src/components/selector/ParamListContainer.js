import {connect} from 'react-redux';
import i18next from 'i18next';

import ParamList from './ParamList';

import {setAddParamId} from '../../actions/selection';

const getParams = params => {
    return [{
        id: 0,
        name: i18next.t('selector:select-parameter')
    }].concat(params);
};

const mapStateToProps = state => {
    return {
        params: getParams(state.params),
        id: state.addParam.id
    };
};

const mapDispatchToProps = dispatch => {
    return {
        onChange: id => {
            dispatch(setAddParamId(id));
        }
    };
};

const ParamListContainer = connect(mapStateToProps, mapDispatchToProps)(ParamList);

export default ParamListContainer;
