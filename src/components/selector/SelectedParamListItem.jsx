import React, {PropTypes} from 'react';
import {translate} from 'react-i18next';

const SelectedParamListItem = props => {
    const {t} = props;

    const style = {
        backgroundColor: props.color
    };

    return (
        <li className="selector-param-list-item uk-panel uk-panel-box">
            <button
                className="selector-param-list-item-remove-button uk-close"
                onClick={() => props.onRemoveClick()}
            ></button>
            <table className="selector-param-list-item-table">
                <tbody>
                <tr><td>{t('sensor')}</td><td>{props.sensor.name}</td></tr>
                <tr><td>{t('station')}</td><td>{props.station.name}</td></tr>
                <tr><td>{t('parameter')}</td><td>{props.param.name}</td></tr>
                </tbody>
            </table>
            <div className="selector-param-list-item-color" style={style}></div>
        </li>
    );
}

SelectedParamListItem.propTypes = {
    id: PropTypes.number.isRequired,
    sensor: PropTypes.shape({
        id: PropTypes.number.isRequired,
        name: PropTypes.string.isRequired
    }),
    station: PropTypes.shape({
        id: PropTypes.number.isRequired,
        name: PropTypes.string.isRequired
    }),
    param: PropTypes.shape({
        id: PropTypes.number.isRequired,
        name: PropTypes.string.isRequired
    }),
    color: PropTypes.string.isRequired,
    onRemoveClick: PropTypes.func.isRequired
};

export default translate(['selector'], { wait: true })(SelectedParamListItem);
