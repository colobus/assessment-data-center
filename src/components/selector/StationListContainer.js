import {connect} from 'react-redux';
import i18next from 'i18next';

import StationList from './StationList';

import {setAddParamStationId} from '../../actions/selection';

const getStations = stations => {
    return [{
        id: 0,
        name: i18next.t('selector:select-station')
    }].concat(stations);
};

const mapStateToProps = state => {
    return {
        stations: getStations(state.stations),
        stationId: state.addParam.stationId
    };
};

const mapDispatchToProps = dispatch => {
    return {
        onChange: id => {
            dispatch(setAddParamStationId(id));
        }
    };
};

const StationListContainer = connect(mapStateToProps, mapDispatchToProps)(StationList);

export default StationListContainer;
