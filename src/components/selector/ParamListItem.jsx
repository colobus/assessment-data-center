import React, {PropTypes} from 'react';

const ParamListItem = ({
    id,
    name
}) => (
    <option className="selector-add-param-list-item" value={id}>{name}</option>
);

ParamListItem.propTypes = {
    id: PropTypes.number.isRequired,
    name: PropTypes.string.isRequired
};

export default ParamListItem;
