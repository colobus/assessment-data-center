import React, {PropTypes} from 'react';

import StationListItem from './StationListItem';

const StationList = ({
    stations,
    stationId,
    onChange
}) => (
    <select
        className="selector-add-param-station-list uk-width-1-1"
        value={stationId}
        onChange={e => onChange(parseInt(e.target.value), 10)}
    >
        {stations.map(station => (
            <StationListItem
                key={station.id}
                {...station}
            />
        ))}
    </select>
);

StationList.propTypes = {
    stations: PropTypes.arrayOf(PropTypes.shape({
        id: PropTypes.number.isRequired,
        name: PropTypes.string.isRequired
    })).isRequired,
    stationId: PropTypes.number.isRequired,
    onChange: PropTypes.func.isRequired
};

export default StationList;
