import React, {PropTypes} from 'react';

const SatelliteListItem = ({
    id,
    name
}) => (
    <option className="selector-satellite-list-item" value={id}>{name}</option>
);

SatelliteListItem.propTypes = {
    id: PropTypes.number.isRequired,
    name: PropTypes.string.isRequired
};

export default SatelliteListItem;
