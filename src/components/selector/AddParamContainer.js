import {connect} from 'react-redux';

import AddParam from './AddParam';

import {toggleAddParamVisibility, addSelectedParamDynamic} from '../../actions/selection';
import {addParamDataDynamic} from '../../actions/paramsData';

const mapStateToProps = state => {
    const {sensorId, stationId, id} = state.addParam;
    return {
        param: {
            sensorId: sensorId,
            stationId: stationId,
            id: id
        },
        showStationList: sensorId > 0,
        showParamList: sensorId > 0,
        showAddButton: sensorId > 0 && stationId > 0 && id > 0
    };
};

const mapDispatchToProps = dispatch => {
    return {
        onAddButtonClick: param => {
            let id = dispatch(addSelectedParamDynamic(param));
            dispatch(addParamDataDynamic(id));
            dispatch(toggleAddParamVisibility(false));
        },
        onCloseClick: () => {
            dispatch(toggleAddParamVisibility(false));
        }
    };
};

const AddParamContainer = connect(mapStateToProps, mapDispatchToProps)(AddParam);

export default AddParamContainer;
