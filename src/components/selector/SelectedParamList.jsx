import React, {PropTypes} from 'react';

import SelectedParamListItem from './SelectedParamListItem';

const SelectedParamList = ({
    params,
    onItemRemoveClick
}) => (
    <ul className="selector-param-list">
        {params.map(param => (
            <SelectedParamListItem
                key={param.id}
                {...param}
                onRemoveClick={() => onItemRemoveClick(param.id)}
            />
        ))}
    </ul>
);

SelectedParamList.propTypes = {
    params: PropTypes.arrayOf(PropTypes.shape({
        id: PropTypes.number.isRequired,
        sensor: PropTypes.shape({
            id: PropTypes.number.isRequired,
            name: PropTypes.string.isRequired
        }),
        station: PropTypes.shape({
            id: PropTypes.number.isRequired,
            name: PropTypes.string.isRequired
        }),
        param: PropTypes.shape({
            id: PropTypes.number.isRequired,
            name: PropTypes.string.isRequired
        }),
        color: PropTypes.string.isRequired
    })),
    onItemRemoveClick: PropTypes.func.isRequired
};

export default SelectedParamList;
