import React, {PropTypes} from 'react';
import {translate} from 'react-i18next';

import SelectedParamListContainer from './SelectedParamListContainer';
import AddParamContainer from './AddParamContainer';

const ParamsFieldset = props => {
    const {t} = props;

    let addParam;
    if (props.showAddParam) {
        addParam = (<AddParamContainer />);
    }
    else {
        addParam = (
            <div className="uk-form-row">
                <button className="uk-button uk-button-primary" onClick={props.onAddClick}>{t('add')}</button>
            </div>
        );
    }

    return (
        <fieldset className="uk-form-stacked uk-margin-bottom">
            <legend>{t('parameters')}</legend>
            <SelectedParamListContainer />
            {addParam}
        </fieldset>
    );
};

ParamsFieldset.propTypes = {
    showAddParam: PropTypes.bool.isRequired,
    onAddClick: PropTypes.func.isRequired
};

export default translate(['selector'], { wait: true })(ParamsFieldset);
