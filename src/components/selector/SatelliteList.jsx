import React, {PropTypes} from 'react';

import SatelliteListItem from './SatelliteListItem';

const SatelliteList = ({ satellites, satelliteId, onChange }) => {
    return (
        <select
            className="selector-satellite-list uk-width-1-1"
            value={satelliteId}
            onChange={e => onChange(parseInt(e.target.value), 10)}
        >
            {satellites.map(satellite => {
                return (
                    <SatelliteListItem
                        key={satellite.id}
                        {...satellite}
                    />
                );
            })}
        </select>
    );
};

SatelliteList.propTypes = {
    satellites: PropTypes.arrayOf(PropTypes.shape({
        id: PropTypes.number.isRequired,
        name: PropTypes.string.isRequired
    })).isRequired,
    satelliteId: PropTypes.number.isRequired,
    onChange: PropTypes.func.isRequired
};

export default SatelliteList;
