import {connect} from 'react-redux';

import SelectedParamList from './SelectedParamList';

import {removeSelectedParam} from '../../actions/selection';
import {removeParamData} from '../../actions/paramsData';

const findItemById = (items, id) => {
    let item = items.find(item => item.id === id);
    return item ? item : null;
};

const getParams = (state) => {
    const {satellites, stations, params, selectedSatelliteId, selectedParams} = state;
    const satellite = findItemById(satellites, selectedSatelliteId);

    return selectedParams.map(selectedParam => {
        return {
            id: selectedParam.id,
            sensor: satellite ? findItemById(satellite.sensors || [], selectedParam.sensorId) : null,
            station: findItemById(stations, selectedParam.stationId),
            param: findItemById(params, selectedParam.paramId),
            color: selectedParam.color
        };
    });
};

const mapStateToProps = state => {
    return {
        params: getParams(state)
    };
};

const mapDispatchToProps = dispatch => {
    return {
        onItemRemoveClick: id => {
            dispatch(removeSelectedParam(id));
            dispatch(removeParamData(id));
        }
    };
};

const SelectedParamListContainer = connect(mapStateToProps, mapDispatchToProps)(SelectedParamList);

export default SelectedParamListContainer;
