import {connect} from 'react-redux';
import i18next from 'i18next';

import SatelliteList from './SatelliteList';

import {setSelectedSatelliteId, removeSelectedParams, toggleAddParamVisibility} from '../../actions/selection';
import {removeParamsData} from '../../actions/paramsData';

const getSatellites = satellites => {
    return [{
        id: 0,
        name: i18next.t('selector:select-satellite')
    }].concat(satellites);
};

const mapStateToProps = state => {
    return {
        satellites: getSatellites(state.satellites),
        satelliteId: state.selectedSatelliteId
    };
};

const mapDispatchToProps = dispatch => {
    return {
        onChange: id => {
            dispatch(setSelectedSatelliteId(id));
            dispatch(removeSelectedParams());
            dispatch(removeParamsData());
            dispatch(toggleAddParamVisibility(false));
        }
    };
};

const SatelliteListContainer = connect(mapStateToProps, mapDispatchToProps)(SatelliteList);

export default SatelliteListContainer;
