import React, {PropTypes} from 'react';

import SensorListItem from './SensorListItem';

const SensorList = ({
    sensors,
    sensorId,
    onChange
}) => (
    <select
        className="selector-add-param-sensor-list uk-width-1-1"
        value={sensorId}
        onChange={e => onChange(parseInt(e.target.value), 10)}
    >
        {sensors.map(sensor => (
            <SensorListItem
                key={sensor.id}
                {...sensor}
            />
        ))}
    </select>
);

SensorList.propTypes = {
    sensors: PropTypes.arrayOf(PropTypes.shape({
        id: PropTypes.number.isRequired,
        name: PropTypes.string.isRequired
    })).isRequired,
    sensorId: PropTypes.number.isRequired,
    onChange: PropTypes.func.isRequired
};

export default SensorList;
