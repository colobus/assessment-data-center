import React, {PropTypes} from 'react';

const SensorListItem = ({
    id,
    name
}) => (
    <option className="selector-add-param-sensor-list-item" value={id}>{name}</option>
);

SensorListItem.propTypes = {
    id: PropTypes.number.isRequired,
    name: PropTypes.string.isRequired
};

export default SensorListItem;
