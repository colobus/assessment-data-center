import React, {PropTypes} from 'react';

const StationListItem = ({
    id,
    name
}) => (
    <option className="selector-add-param-station-list-item" value={id}>{name}</option>
);

StationListItem.propTypes = {
    id: PropTypes.number.isRequired,
    name: PropTypes.string.isRequired
};

export default StationListItem;
