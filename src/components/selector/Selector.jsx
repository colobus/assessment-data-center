import React, {PropTypes} from 'react';

import SatelliteFieldset from './SatelliteFieldset';
import ParamsFieldsetContainer from './ParamsFieldsetContainer';

import './selector.less';

const Selector = ({
    showParams
}) => {
    let paramsFieldset;
    if (showParams) {
        paramsFieldset = (<ParamsFieldsetContainer />);
    }

    return (
        <div className="selector">
            <form className="uk-form" onSubmit={e => e.preventDefault()}>
                <SatelliteFieldset />
                {paramsFieldset}
            </form>
        </div>
    );
};

Selector.propTypes = {
    showParams: PropTypes.bool.isRequired
};

export default Selector;
