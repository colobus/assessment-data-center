import React, {PropTypes} from 'react';
import {translate} from 'react-i18next';

import SensorListContainer from './SensorListContainer';
import StationListContainer from './StationListContainer';
import ParamListContainer from './ParamListContainer';

const AddParam = props => {
    const {t} = props;

    const stationList = props.showStationList ? (
        <div className="uk-form-row">
            <label className="uk-form-label">{t('station')}</label>
            <StationListContainer />
        </div>
    ) : null;

    const paramList = props.showParamList ? (
        <div className="uk-form-row">
            <label className="uk-form-label">{t('parameter')}</label>
            <ParamListContainer />
        </div>
    ) : null;

    const addButton = props.showAddButton ? (
        <div className="uk-form-row">
            <button
                className="uk-button uk-button-primary"
                onClick={() => props.onAddButtonClick(props.param)}
            >{t('add')}</button>
        </div>
    ) : null;

    return (
        <div className="selector-add-param uk-panel uk-panel-box">
            <button className="selector-add-param-close uk-close" onClick={props.onCloseClick}></button>
            <div className="uk-form-row">
                <label className="uk-form-label">{t('sensor')}</label>
                <SensorListContainer />
            </div>
            {stationList}
            {paramList}
            {addButton}
        </div>
    );
};

AddParam.propTypes = {
    param: PropTypes.shape({
        sensorId: PropTypes.number.isRequired,
        stationId: PropTypes.number.isRequired,
        id: PropTypes.number.isRequired
    }).isRequired,
    showStationList: PropTypes.bool.isRequired,
    showParamList: PropTypes.bool.isRequired,
    showAddButton: PropTypes.bool.isRequired,
    onAddButtonClick: PropTypes.func.isRequired,
    onCloseClick: PropTypes.func.isRequired
};

export default translate(['selector'], { wait: true })(AddParam);
