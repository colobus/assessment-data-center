import {connect} from 'react-redux';

import Selector from './Selector';

const mapStateToProps = state => {
    return {
        showParams: state.selectedSatelliteId > 0
    };
};

const mapDispatchToProps = () => {
    return {};
};

const SelectorContainer = connect(mapStateToProps, mapDispatchToProps)(Selector);

export default SelectorContainer;
