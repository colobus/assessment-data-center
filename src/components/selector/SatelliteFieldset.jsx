import React from 'react';
import {translate} from 'react-i18next';

import SatelliteListContainer from './SatelliteListContainer';

const SatelliteFieldset = props => {
    const {t} = props;

    return (
        <fieldset className="uk-form-stacked uk-margin-bottom">
            <legend>{t('satellite')}</legend>
            <div className="uk-form-row">
                <SatelliteListContainer />
            </div>
        </fieldset>
    );
};

export default translate(['selector'], { wait: true })(SatelliteFieldset);
