import React, {PropTypes} from 'react';

import ParamListItem from './ParamListItem';

const ParamList = ({
    params,
    id,
    onChange
}) => (
    <select
        className="selector-add-param-list uk-width-1-1"
        value={id}
        onChange={e => onChange(parseInt(e.target.value, 10))}
    >
        {params.map(param => (
            <ParamListItem
                key={param.id}
                {...param}
            />
        ))}
    </select>
);

ParamList.propTypes = {
    params: PropTypes.arrayOf(PropTypes.shape({
        id: PropTypes.number.isRequired,
        name: PropTypes.string.isRequired
    })).isRequired,
    id: PropTypes.number.isRequired,
    onChange: PropTypes.func.isRequired
};

export default ParamList;
