import React from 'react';

import HeaderContainer from '../header/HeaderContainer';
import SelectorContainer from '../selector/SelectorContainer';
import ComparatorContainer from '../comparator/ComparatorContainer';

import './application.less';

const Application = () => (
    <div className="application">
        <HeaderContainer />
        <SelectorContainer />
        <ComparatorContainer />
    </div>
);

export default Application;
