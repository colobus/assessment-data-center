import {connect} from 'react-redux';

import Header from './Header';

import {showModal} from '../../actions/modal';

const mapStateToProps = () => {
    return {};
};

const mapDispatchToProps = dispatch => {
    return {
        onNavItemClick: name => {
            switch (name) {
                case 'settings': {
                    dispatch(showModal('settings'));
                    break;
                }
            }
        }
    };
};

const HeaderContainer = connect(mapStateToProps, mapDispatchToProps)(Header);

export default HeaderContainer;
