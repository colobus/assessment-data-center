import {connect} from 'react-redux';

import LanguageSwitcher from './LanguageSwitcher';

import {setLanguage} from '../../actions/language';

const mapStateToProps = state => {
    return {
        language: state.language
    };
};

const mapDispatchToProps = dispatch => {
    return {
        onItemClick: language => {
            dispatch(setLanguage(language));
        }
    };
};

const LanguageSwitcherContainer = connect(mapStateToProps, mapDispatchToProps)(LanguageSwitcher);

export default LanguageSwitcherContainer;
