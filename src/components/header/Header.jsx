import React, {PropTypes} from 'react';
import {translate} from 'react-i18next';

import LanguageSwitcherContainer from './LanguageSwitcherContainer';

import './header.less';

const Header = props => {
    const {t} = props;

    return (
        <div className="header">
            <nav className="uk-navbar uk-navbar-attached">
                <a className="uk-navbar-brand">{t('name')}</a>
                <div className="uk-navbar-flip">
                    <ul className="uk-navbar-nav">
                        <li><a onClick={() => props.onNavItemClick('settings')}>{t('settings')}</a></li>
                        <LanguageSwitcherContainer />
                    </ul>
                </div>
            </nav>
        </div>
    );
};

Header.propTypes = {
    onNavItemClick: PropTypes.func.isRequired
};

export default translate(['application', 'header'], { wait: true })(Header);
