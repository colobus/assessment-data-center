import _map from 'lodash/map';
import _filter from 'lodash/filter';

import React, {PropTypes} from 'react';

import {translate} from 'react-i18next';
import i18next from 'i18next';

import '../../uikit/uikit';

const LanguageSwitcher = props => {
    const {t} = props;

    const languages = _map(
        _filter(i18next.options.whitelist, language => language !== 'cimode'),
        language => {
            return {
                id: language,
                active: language === props.language
            }
        }
    );

    return (
        <li className="uk-parent uk-dropdown-close" data-uk-dropdown="{ mode: 'click' }">
            <a>{t('language')} <i className="uk-icon-caret-down"></i></a>
            <div className="uk-dropdown uk-dropdown-navbar">
                <ul className="uk-nav uk-nav-side uk-nav-navbar">
                    {languages.map(language => (
                        <li
                            key={language.id}
                            className={language.active ? 'uk-active' : ''}
                            onClick={() => props.onItemClick(language.id)}
                        >
                            <a>{t(language.id)}</a>
                        </li>
                    ))}
                </ul>
            </div>
        </li>
    );
};

LanguageSwitcher.propTypes = {
    language: PropTypes.string.isRequired,
    onItemClick: PropTypes.func.isRequired
};

export default translate(['language'], { wait: true })(LanguageSwitcher);
