import {connect} from 'react-redux';

import Settings from './Settings';

import {
    setGraphType,
    setGraphThickness
} from '../../actions/graph';

const mapStateToProps = state => {
    return {
        graphType: state.graph.type,
        graphThickness: state.graph.thickness
    };
};

const mapDispatchToProps = dispatch => {
    return {
        setGraphType: type => {
            dispatch(setGraphType(type));
        },
        setGraphThickness: thickness => {
            dispatch(setGraphThickness(thickness));
        }
    };
};

const SettingsContainer = connect(mapStateToProps, mapDispatchToProps)(Settings);

export default SettingsContainer;
