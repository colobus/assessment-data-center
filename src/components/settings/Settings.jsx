import React, {PropTypes} from 'react';
import {translate} from 'react-i18next';

const graphTypes = ['straight', 'smoothed'];
const graphThickness = ['thin', 'medium', 'fat'];

const Settings = props => {
    const {t} = props;

    return (
        <div className="uk-modal-dialog">
            <div className="uk-modal-close uk-close"></div>
            <div className="uk-modal-header">
                <h2>{t('settings')}</h2>
            </div>
            <div>
                <form className="uk-form uk-form-stacked" onSubmit={e => e.preventDefault()}>
                    <fieldset>
                        <legend>{t('graphs')}</legend>
                        <div className="uk-form-row">
                            <label className="uk-form-label">{t('type')}</label>
                            <div className="uk-form-controls">
                                <select
                                    className="uk-width-1-1"
                                    value={props.graphType}
                                    onChange={e => props.setGraphType(e.target.value)}
                                >
                                    {graphTypes.map(graphType => (
                                        <option key={graphType} value={graphType}>{t(graphType)}</option>
                                    ))}
                                </select>
                            </div>
                        </div>
                        <div className="uk-form-row">
                            <label className="uk-form-label">{t('thickness')}</label>
                            <div className="uk-form-controls">
                                <select
                                    className="uk-width-1-1"
                                    value={props.graphThickness}
                                    onChange={e => props.setGraphThickness(e.target.value)}
                                >
                                    {graphThickness.map(graphThickness => (
                                        <option key={graphThickness} value={graphThickness}>{t(graphThickness)}</option>
                                    ))}
                                </select>
                            </div>
                        </div>
                    </fieldset>
                </form>
            </div>
            <div className="uk-modal-footer uk-text-right">
                <button type="button" className="uk-button" onClick={() => props.hide()}>{t('close')}</button>
            </div>
        </div>
    );
};

Settings.propTypes = {
    graphType: PropTypes.string.isRequired,
    graphThickness: PropTypes.string.isRequired,
    setGraphType: PropTypes.func.isRequired,
    setGraphThickness: PropTypes.func.isRequired,
    hide: PropTypes.func
};

export default translate(['settings', 'graph'])(Settings);
