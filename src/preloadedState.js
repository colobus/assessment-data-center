import _each from 'lodash/each';
import merge from 'merge-professor';

const iso8601RegExp = /^\d{4}-\d{2}-\d{2}T\d{2}:\d{2}:\d{2}\.\d{3}Z$/;

const initializeDates = object => {
    _each(object, (value, key) => {
        if (value !== null && (typeof value === 'object')) {
            initializeDates(value);
        }
        else if (typeof value === 'string' && iso8601RegExp.test(value)) {
            object[key] = new Date(value);
        }
    });
};

const defaultPreloadedState = {
    satellites: [{
        id: 25994,
        name: 'Terra',
        sensors: [{
            id: 1,
            name: 'MODIS'
        }]
    }, {
        id: 27424,
        name: 'Aqua',
        sensors: [{
            id: 1,
            name: 'MODIS'
        }]
    }, {
        id: 40069,
        name: 'Meteor-M №2',
        sensors: [{
            id: 2,
            name: 'MSU-MR'
        }, {
            id: 3,
            name: 'KMSS'
        }, {
            id: 4,
            name: 'BRLK'
        }, {
            id: 5,
            name: 'IKFS'
        }, {
            id: 6,
            name: 'MTVZA'
        }]
    }],
    stations: [{
        id: 1,
        name: 'P0'
    }, {
        id: 2,
        name: 'P1'
    }, {
        id: 3,
        name: 'P2'
    }, {
        id: 4,
        name: 'P3'
    }],
    params: [{
        id: 1,
        name: 'Parameter 1'
    }, {
        id: 2,
        name: 'Parameter 2'
    }, {
        id: 3,
        name: 'Parameter 3'
    }, {
        id: 4,
        name: 'Parameter 4'
    }, {
        id: 5,
        name: 'Parameter 5'
    }, {
        id: 6,
        name: 'Parameter 6'
    }, {
        id: 7,
        name: 'Parameter 7'
    }]
};

let state = null;

if (localStorage.getItem('version') === VERSION) {
    try {
        state = JSON.parse(localStorage.getItem('state'));
    }
    catch (e) {
        // Unable to parse state from local storage
    }
}

let preloadedState = state === null ? defaultPreloadedState : merge(defaultPreloadedState, state, 'id');

if (preloadedState.modal) {
    preloadedState.modal.animating = false;
}

initializeDates(preloadedState);

export default preloadedState;
