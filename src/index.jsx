import './index.less';

import React from 'react';
import {render} from 'react-dom';
import {Provider} from 'react-redux';
import {I18nextProvider} from 'react-i18next';

import store from './store';
import i18n from './i18n';

import Application from './components/application/Application';
import ModalContainer from './components/modal/ModalContainer';

render(
    <Provider store={store}>
        <I18nextProvider i18n={i18n}>
            <div>
                <Application />
                <ModalContainer />
            </div>
        </I18nextProvider>
    </Provider>, document.querySelector('.container')
);
