import {createStore, applyMiddleware} from 'redux';
import thunkMiddleware from 'redux-thunk';
import createLogger from 'redux-logger';

import reducer from './reducers';
import initialState from './preloadedState';

const loggerMiddleware = createLogger();

const store = createStore(reducer, initialState, applyMiddleware(
    thunkMiddleware,
    loggerMiddleware
));

console.log(store.getState());

store.subscribe(() => {
    localStorage.setItem('state', JSON.stringify(store.getState()));
    localStorage.setItem('version', VERSION);
});

export default store;
