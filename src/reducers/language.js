import {SET_LANGUAGE} from '../constants/actionTypes';

export default (state = 'en', action) => {
    switch (action.type) {
        case SET_LANGUAGE: {
            return action.language;
        }

        default: {
            return state;
        }
    }
};
