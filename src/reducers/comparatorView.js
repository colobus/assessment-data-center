import {
    SET_COMPARATOR_VIEW
} from '../constants/actionTypes';

const comparatorView = (state = 'united', action) => {
    switch (action.type) {
        case SET_COMPARATOR_VIEW: {
            return action.view;
        }

        default: {
            return state;
        }
    }
};

export default comparatorView;
