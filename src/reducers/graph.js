import {
    SET_GRAPH_TYPE,
    SET_GRAPH_THICKNESS
} from '../constants/actionTypes';

const graph = (state = {
    type: 'straight',
    thickness: 'medium'
}, action) => {
    switch (action.type) {
        case SET_GRAPH_TYPE: {
            return Object.assign({}, state, {
                type: action.graphType
            });
        }

        case SET_GRAPH_THICKNESS: {
            return Object.assign({}, state, {
                thickness: action.graphThickness
            });
        }

        default: {
            return state;
        }
    }
};

export default graph;
