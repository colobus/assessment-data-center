import {
    ADD_PARAM_DATA,
    REMOVE_PARAM_DATA,
    REMOVE_PARAMS_DATA
} from '../constants/actionTypes';

const paramData = (state = {}, action) => {
    switch (action.type) {
        case ADD_PARAM_DATA: {
            return Object.assign({}, state, {
                [action.id]: action.data
            });
        }

        case REMOVE_PARAM_DATA: {
            if (!state[action.id]) {
                return state;
            }
            let newState = Object.assign({}, state);
            delete newState[action.id];
            return newState;
        }

        case REMOVE_PARAMS_DATA: {
            return {
                time: state.time
            };
        }

        default: {
            return state;
        }
    }
};

export default paramData;
