const selectedSatelliteId = (state = 0, action) => {
    switch (action.type) {
        case 'SET_SELECTION_SATELLITE_ID': {
            return action.id;
        }

        default: {
            return state;
        }
    }
};

export default selectedSatelliteId;
