import {combineReducers} from 'redux';

import language from './language';
import graph from './graph';
import modal from './modal';
import selectedSatelliteId from './selectedSatelliteId';
import selectedParams from './selectedParams';
import addParam from './addParam';
import comparatorView from './comparatorView';
import paramsData from './paramsData';

const defaultArrayReducer = (state = []) => {
    return state;
};

const reducer = combineReducers({
    satellites: defaultArrayReducer,
    stations: defaultArrayReducer,
    params: defaultArrayReducer,
    language,
    graph,
    modal,
    selectedSatelliteId,
    selectedParams,
    addParam,
    comparatorView,
    paramsData
});

export default reducer;
