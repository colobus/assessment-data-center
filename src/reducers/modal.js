import {
    SHOW_MODAL,
    HIDE_MODAL,
    START_MODAL_ANIMATION,
    STOP_MODAL_ANIMATION
} from '../constants/actionTypes';

const modal = (state = {
    name: null,
    animating: false
}, action) => {
    switch (action.type) {
        case SHOW_MODAL: {
            return Object.assign({}, state, {
                name: action.name
            });
        }

        case HIDE_MODAL: {
            return Object.assign({}, state, {
                name: null
            });
        }

        case START_MODAL_ANIMATION: {
            return Object.assign({}, state, {
                animating: true
            });
        }

        case STOP_MODAL_ANIMATION: {
            return Object.assign({}, state, {
                animating: false
            });
        }

        default: {
            return state;
        }
    }
};

export default modal;
