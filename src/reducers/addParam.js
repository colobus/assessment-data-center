import {
    TOGGLE_ADD_PARAM_VISIBILITY,
    SET_ADD_PARAM_SENSOR_ID,
    SET_ADD_PARAM_STATION_ID,
    SET_ADD_PARAM_ID
} from '../constants/actionTypes';

const addParam = (state = {
    show: false,
    sensorId: 0,
    stationId: 0,
    id: 0
}, action) => {
    switch (action.type) {
        case TOGGLE_ADD_PARAM_VISIBILITY: {
            return Object.assign({}, state, {
                show: action.visible
            }, action.visible ? {} : {
                sensorId: 0,
                stationId: 0,
                id: 0
            });
        }

        case SET_ADD_PARAM_SENSOR_ID: {
            return Object.assign({}, state, {
                sensorId: action.id,
                stationId: 0,
                id: 0
            });
        }

        case SET_ADD_PARAM_STATION_ID: {
            return Object.assign({}, state, {
                stationId: action.id
            });
        }

        case SET_ADD_PARAM_ID: {
            return Object.assign({}, state, {
                id: action.id
            });
        }

        default: {
            return state;
        }
    }
};

export default addParam;
