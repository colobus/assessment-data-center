const selectedParam = (state = {}, action) => {
    switch (action.type) {
        case 'ADD_SELECTION_PARAM': {
            return {
                id: action.id,
                sensorId: action.sensorId,
                stationId: action.stationId,
                paramId: action.paramId,
                color: action.color
            };
        }

        default: {
            return state;
        }
    }
};

const selectedParams = (state = [], action) => {
    switch (action.type) {
        case 'ADD_SELECTION_PARAM': {
            return [
                ...state,
                selectedParam(undefined, action)
            ];
        }

        case 'REMOVE_SELECTION_PARAM': {
            let i = state.findIndex(param => {
                return param.id === action.id;
            });

            return i >= 0 ? [
                ...state.slice(0, i),
                ...state.slice(i + 1)
            ] : state;
        }

        case 'REMOVE_SELECTION_PARAMS': {
            return [];
        }

        default: {
            return state;
        }
    }
};

export default selectedParams;
