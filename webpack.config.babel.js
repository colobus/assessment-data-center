import path from 'path';
import webpack from 'webpack';
import HtmlPlugin from 'html-webpack-plugin';
import ExtractTextPlugin from 'extract-text-webpack-plugin';
import autoprefixer from 'autoprefixer';

import packageJson from './package.json';

const NODE_ENV = process.env.NODE_ENV === 'production' ? 'production' : 'development';

let config = {
    context: path.resolve(__dirname, 'src'),
    entry: {
        index: './index.jsx'
    },
    output: {
        path: path.resolve(__dirname, 'dist'),
        filename: 'js/[name]' + (NODE_ENV === 'production' ? '.[hash:6]' : '') + '.js'
    },
    module: {
        loaders: [{
            test: /\.jsx?$/,
            loader: 'babel',
            include: [
                path.resolve(__dirname, 'src'),
            ],
            exclude: [
                path.resolve(__dirname, 'src/lib')
            ]
        }, {
            test: /\.json$/,
            loader: 'json',
            include: [
                path.resolve(__dirname, 'src'),
            ]
        }, {
            test: /amcharts\.js$/,
            loader: 'exports?window.AmCharts',
            include: [
                path.resolve(__dirname, 'src/lib/amcharts')
            ]
        }, {
            test: /\.less$/,
            loader: ExtractTextPlugin.extract('style', 'css!postcss!less', {
                publicPath: '../'
            }),
            include: [
                path.resolve(__dirname, 'src')
            ]
        }, {
            test: /\.(ttf|otf|woff2?)$/,
            loader: 'file?name=fonts/[name].[hash:6].[ext]',
            include: [
                path.resolve(__dirname, 'node_modules/uikit/src/fonts')
            ]
        }]
    },
    resolve: {
        modulesDirectories: ['node_modules'],
        extensions: ['', '.js', '.jsx'],
        alias: {
            amcharts: path.resolve(__dirname, 'src/lib/amcharts')
        }
    },
    resolveLoader: {
        modulesDirectories: ['node_modules'],
        moduleTemplates: ['*-loader', '*'],
        extensions: ['', '.js']
    },
    plugins: [
        new webpack.DefinePlugin({
            'process.env': {
                NODE_ENV: JSON.stringify(NODE_ENV)
            },
            NODE_ENV: JSON.stringify(NODE_ENV),
            VERSION: JSON.stringify(packageJson.version)
        }),
        new webpack.ProvidePlugin({
            $: 'jquery',
            jQuery: 'jquery',
            'window.jQuery': 'jquery'
        }),
        new webpack.IgnorePlugin(/vertx/),
        new webpack.NoErrorsPlugin(),
        new HtmlPlugin({
            template: path.resolve(__dirname, 'src/index.mustache'),
            filename: path.resolve(__dirname, 'dist/index.html')
        }),
        new ExtractTextPlugin('css/[name]' + (NODE_ENV === 'production' ? '.[hash:6]' : '') + '.css', {
            allChunks: true
        })
    ],
    postcss: () => {
        return [autoprefixer];
    },
    devtool: NODE_ENV === 'development' ? 'source-map' : null
};

if (NODE_ENV === 'production') {
    config.plugins.push(new webpack.optimize.UglifyJsPlugin({
        compress: {
            warnings: false,
            drop_console: false,
            unsafe: true
        },
        sourceMap: false
    }));
}

export default config;
